# Software License Agreement

**ipSCAPE Cumulus** – https://bitbucket.org/ipscapeau/ipscape-component-library <br>
Copyright (c) 2021, [ipSCAPE](https://ipscape.com). All rights reserved.

Licensed under the terms of [GNU General Public License Version 2 or later](http://www.gnu.org/licenses/gpl.html).

## Sources of Intellectual Property Included in CKEditor

Where not otherwise indicated, all ipSCAPE content is authored by ipSCAPE engineers and consists of ipSCAPE-owned intellectual property. In some specific instances, ipSCAPE will incorporate work done by developers outside of ipSCAPE with their express permission.

## Trademarks

**ipSCAPE** is a trademark of [ipSCAPE](https://ipscape.com). All other brand and product names are trademarks, registered trademarks or service marks of their respective holders.
