# ipSCAPE - CKEditor 5 custom build

## Quick start

First, install the build from npm:

```bash
npm install --save @ipscape/ckeditor5-build
```

or

```bash
yarn add @ipscape/ckeditor5-build
```

And use it in your website:

```html
<div id="editor">
  <p>This is the editor content.</p>
</div>
<script src="./node_modules/@ipscape/ckeditor5-build/build/ckeditor.js"></script>
<script>
  ClassicEditor.create(document.querySelector("#editor"))
    .then((editor) => {
      window.editor = editor;
    })
    .catch((error) => {
      console.error("There was a problem initializing the editor.", error);
    });
</script>
```

Or in your JavaScript application:

```js
import ClassicEditor from "@ipscape/ckeditor5-build";

// Or using the CommonJS version:
// const ClassicEditor = require( '@ipscape/ckeditor5-build' );

ClassicEditor.create(document.querySelector("#editor"))
  .then((editor) => {
    window.editor = editor;
  })
  .catch((error) => {
    console.error("There was a problem initializing the editor.", error);
  });
```

## Extract css styles

https://ckeditor.com/docs/ckeditor5/latest/builds/guides/integration/advanced-setup.html#option-minifying-javascript

## html support
```html
<script>
    IpscapeEditor.create(document.querySelector("#editor"), {
        htmlSupport: {
            allow: [
                {
                    name: /.*/,
                    attributes: true,
                    classes: true,
                    styles: true,
                },
            ],
        },
    })
            .then((editor) => {
                window.editor = editor;
            })
            .catch((error) => {
                console.error("There was a problem initializing the editor.", error);
            });
</script>
```

## dynamic fields 
```html
 <script>
    IpscapeEditor.create(document.querySelector("#editor"), {
        // dynamic fields example
        dynamicFields: [
            { name: "%FIELD_4ET%", label: "Order Name" },
            { name: "%FIELD_7ET%", label: "Order Telephone Number" },
            { name: "%FIELD_5ET%", label: "Order Date" },
            { name: "%FIELD_9ET%", label: "Order Delivery Address" },
        ],
        systemVariables: [
            { name: "[agent_last_name]", label: "Agent last name" },
            { name: "[agent_first_name]", label: "Agent first name" },
            { name: "[organisation_title]", label: "Organisation title" },
            { name: "[lead_id]", label: "Lead id" },
        ],
    })
            .then((editor) => {
                window.editor = editor;
            })
            .catch((error) => {
                console.error("There was a problem initializing the editor.", error);
            });
</script>
```

## Build project

```bash
yarn build
```

Check /sample/index.html for testing the new build.

## License

Licensed under the terms of [GNU General Public License Version 2 or later](http://www.gnu.org/licenses/gpl.html). For full details about the license, please check the `LICENSE.md`
