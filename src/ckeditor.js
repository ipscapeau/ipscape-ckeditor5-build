/**
 * @license Copyright (c) 2003-2021, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

// The editor creator to use.
import ClassicEditorBase from "@ckeditor/ckeditor5-editor-classic/src/classiceditor";

import Essentials from "@ckeditor/ckeditor5-essentials/src/essentials";
import Alignment from "@ckeditor/ckeditor5-alignment/src/alignment";
import Autoformat from "@ckeditor/ckeditor5-autoformat/src/autoformat";
import Bold from "@ckeditor/ckeditor5-basic-styles/src/bold";
import BlockQuote from "@ckeditor/ckeditor5-block-quote/src/blockquote";
import CKFinder from "@ckeditor/ckeditor5-ckfinder/src/ckfinder";
import CloudServices from "@ckeditor/ckeditor5-cloud-services/src/cloudservices";
import EasyImage from "@ckeditor/ckeditor5-easy-image/src/easyimage";
import FindAndReplace from '@ckeditor/ckeditor5-find-and-replace/src/findandreplace';
import Font from "@ckeditor/ckeditor5-font/src/font";
import GeneralHtmlSupport from '@ckeditor/ckeditor5-html-support/src/generalhtmlsupport';
import Heading from "@ckeditor/ckeditor5-heading/src/heading";
import HorizontalLine from "@ckeditor/ckeditor5-horizontal-line/src/horizontalline";
import Image from "@ckeditor/ckeditor5-image/src/image";
import ImageBlock from "@ckeditor/ckeditor5-image/src/imageblock";
import ImageCaption from "@ckeditor/ckeditor5-image/src/imagecaption";
import ImageInsert from "@ckeditor/ckeditor5-image/src/imageinsert";
import ImageInline from "@ckeditor/ckeditor5-image/src/imageinline";
import ImageResize from "@ckeditor/ckeditor5-image/src/imageresize";
import ImageStyle from "@ckeditor/ckeditor5-image/src/imagestyle";
import ImageToolbar from "@ckeditor/ckeditor5-image/src/imagetoolbar";
import ImageUpload from "@ckeditor/ckeditor5-image/src/imageupload";
import Indent from "@ckeditor/ckeditor5-indent/src/indent";
import Italic from "@ckeditor/ckeditor5-basic-styles/src/italic";
import Link from "@ckeditor/ckeditor5-link/src/link";
import LinkImage from "@ckeditor/ckeditor5-link/src/linkimage";
import List from "@ckeditor/ckeditor5-list/src/list";
import MediaEmbed from "@ckeditor/ckeditor5-media-embed/src/mediaembed";
import Paragraph from "@ckeditor/ckeditor5-paragraph/src/paragraph";
import PasteFromOffice from "@ckeditor/ckeditor5-paste-from-office/src/pastefromoffice";
import RemoveFormat from '@ckeditor/ckeditor5-remove-format/src/removeformat';
import SourceEditing from "@ckeditor/ckeditor5-source-editing/src/sourceediting";
import SpecialCharactersEssentials from "@ckeditor/ckeditor5-special-characters/src/specialcharactersessentials";
import SpecialCharacters from "@ckeditor/ckeditor5-special-characters/src/specialcharacters";
import Strikethrough from "@ckeditor/ckeditor5-basic-styles/src/strikethrough";
import Table from "@ckeditor/ckeditor5-table/src/table";
import TableToolbar from "@ckeditor/ckeditor5-table/src/tabletoolbar";
import TextTransformation from "@ckeditor/ckeditor5-typing/src/texttransformation";
import Underline from "@ckeditor/ckeditor5-basic-styles/src/underline";
import UploadAdapter from "@ckeditor/ckeditor5-adapter-ckfinder/src/uploadadapter";

// custom plugins
import IpsDynamicFields from "./ips-dynamic-fields/ips-dynamic-fields";
import IpsFileDialog from "./ips-file-dialog/ips-file-dialog";
import IpsImageAttach from "./ips-image-attach/ips-image-attach";
import IpsImageInsert from "./ips-image-insert/ips-image-insert";
import IpsSelectFilesDropdown from "./ips-select-files-dropdown/ips-select-files-dropdown";
import IpsSystemVariables from "./ips-system-variables/ips-system-variables";

export default class IpscapeEditor extends ClassicEditorBase {}

// Plugins to include in the build.
IpscapeEditor.builtinPlugins = [
  Essentials,
  Alignment,
  Autoformat,
  Bold,
  BlockQuote,
  CKFinder,
  CloudServices,
  EasyImage,
  FindAndReplace,
  Font,
  GeneralHtmlSupport,
  Heading,
  HorizontalLine,
  Italic,
  Image,
  ImageBlock,
  ImageCaption,
  ImageInline,
  ImageInsert,
  ImageResize,
  ImageStyle,
  ImageToolbar,
  ImageUpload,
  Indent,
  Link,
  LinkImage,
  List,
  MediaEmbed,
  Paragraph,
  PasteFromOffice,
  RemoveFormat,
  SourceEditing,
  SpecialCharactersEssentials,
  SpecialCharacters,
  Strikethrough,
  Table,
  TableToolbar,
  TextTransformation,
  Underline,
  UploadAdapter,
  // custom plugins
  IpsDynamicFields,
  IpsFileDialog,
  IpsImageAttach,
  IpsImageInsert,
  IpsSelectFilesDropdown,
  IpsSystemVariables,
];

// Editor configuration.
IpscapeEditor.defaultConfig = {
  toolbar: {
    items: [
      "undo",
      "redo",
      "|",
      "heading",
      "|",
      "fontFamily",
      "|",
      "fontSize",
      "|",
      "bold",
      "italic",
      "underline",
      "strikethrough",
      "removeFormat",
      "|",
      "fontColor",
      "fontBackgroundColor",
      "|",
      "alignment",
      "|",
      "bulletedList",
      "numberedList",
      "|",
      "insertTable",
      "horizontalLine",
      "specialCharacters",
      "uploadImage",
      "insertImage",
      "link",
      "mediaEmbed",
      "sourceEditing",
      "findAndReplace",
      "|",
      "ipsImageInsert",
      "ipsSelectFilesDropdown",
      "|",
      "ipsDynamicFields",
      "|",
      "ipsSystemVariables",
    ],
  },
  image: {
    styles: {
      options: [
        "inline",
        "block",
        "side",
        {
          name: "alignLeft",
          title: "Left aligned image",
          modelElements: ["imageBlock", "imageInline"],
          className: "image-style-align-left",
        },
        {
          name: "alignCenter",
          title: "Center aligned image",
          modelElements: ["imageBlock", "imageInline"],
          className: "image-style-align-center",
        },
        {
          name: "alignRight",
          title: "Right aligned image",
          modelElements: ["imageBlock", "imageInline"],
          className: "image-style-align-right",
        },
      ],
    },
    resizeOptions: [
      {
        name: "resizeImage:original",
        label: "Original",
        value: null,
      },
      {
        name: "resizeImage:25",
        label: "25%",
        value: "25",
      },
      {
        name: "resizeImage:50",
        label: "50%",
        value: "50",
      },
      {
        name: "resizeImage:75",
        label: "75%",
        value: "75",
      },
      {
        name: "resizeImage:100",
        label: "100%",
        value: "100",
      },
    ],
    toolbar: [
      "linkImage",
      "imageTextAlternative",
      "|",
      "imageStyle:alignLeft",
      "imageStyle:alignCenter",
      "imageStyle:alignRight",
      "|",
      "resizeImage",
      "|",
      "imageStyle:inline",
      "imageStyle:block",
      "imageStyle:side",
    ],
  },
  table: {
    contentToolbar: ["tableColumn", "tableRow", "mergeTableCells"],
  },
  // This value must be kept in sync with the language defined in webpack.config.js.
  language: "en",
};
