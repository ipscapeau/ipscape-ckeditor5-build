import Command from "@ckeditor/ckeditor5-core/src/command";

export default class IpsDynamicFieldsCommand extends Command {
  execute(dynamicField) {
    const editor = this.editor;

    editor.model.change((writer) => {
      const insertPosition = editor.model.document.selection.getFirstPosition();
      writer.insertText(dynamicField.name, insertPosition);
    });
  }
}
