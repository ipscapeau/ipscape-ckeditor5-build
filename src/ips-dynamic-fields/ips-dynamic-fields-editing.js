import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import IpsDynamicFieldsCommand from "./ips-dynamic-fields-command";

export default class IpsDynamicFieldsEditing extends Plugin {
  init() {
    this.editor.commands.add(
      "insertDynamicField",
      new IpsDynamicFieldsCommand(this.editor)
    );
  }
}
