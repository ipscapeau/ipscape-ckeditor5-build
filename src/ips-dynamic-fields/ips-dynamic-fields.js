import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import IpsDynamicFieldsUi from "./ips-dynamic-fields-ui";
import IpsDynamicFieldsEditing from "./ips-dynamic-fields-editing";

/**
 * This plugin creates a dynamic fields dropdown
 */
export default class IpsDynamicFields extends Plugin {
  static get requires() {
    return [IpsDynamicFieldsEditing, IpsDynamicFieldsUi];
  }
}
