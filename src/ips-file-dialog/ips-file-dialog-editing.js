import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import IpsFileDialogCommand from "./ips-file-dialog-command";

export default class IpsFileDialogEditing extends Plugin {
  init() {
    this.editor.commands.add(
      "selectFiles",
      new IpsFileDialogCommand(this.editor)
    );
  }
}
