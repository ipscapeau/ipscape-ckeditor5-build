import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import FileDialogButtonView from "@ckeditor/ckeditor5-upload/src/ui/filedialogbuttonview";
import imageIcon from "../ips-image-insert/theme/icons/image.svg";

export default class IpsFileDialogUi extends Plugin {
  init() {
    const editor = this.editor;
    const componentFactory = editor.ui.componentFactory;
    const t = editor.t;

    componentFactory.add("ipsFileDialog", (locale) => {
      const command = editor.commands.get("selectFiles");
      const button = new FileDialogButtonView(locale);

      button.set({
        acceptedType: "*/*",
        allowMultipleFiles: true,
      });

      button.buttonView.set({
        label: t("Insert image"),
        icon: imageIcon,
        tooltip: true,
      });

      button.bind("isEnabled").to(command);

      button.on("done", (evt, files) => {
        editor.execute("selectFiles", Array.from(files));
      });

      return button;
    });
  }
}
