import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import IpsFileDialogUi from "./ips-file-dialog-ui";
import IpsFileDialogEditing from "./ips-file-dialog-editing";

/**
 * This plugin creates a file dialog and returns file array in custom event
 */
export default class IpsFileDialog extends Plugin {
  static get requires() {
    return [IpsFileDialogEditing, IpsFileDialogUi];
  }
}
