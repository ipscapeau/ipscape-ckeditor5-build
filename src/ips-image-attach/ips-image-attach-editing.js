import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import IpsImageAttachCommand from "./ips-image-attach-command";

export default class IpsImageAttachEditing extends Plugin {
  init() {
    this.editor.commands.add(
      "attachImage",
      new IpsImageAttachCommand(this.editor)
    );
  }
}
