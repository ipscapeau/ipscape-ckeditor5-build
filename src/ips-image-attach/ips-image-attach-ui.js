import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import ButtonView from "@ckeditor/ckeditor5-ui/src/button/buttonview";

import imageIcon from "./theme/icons/browse-files.svg";

export default class IpsImageAttachUi extends Plugin {
  init() {
    const editor = this.editor;
    const componentFactory = editor.ui.componentFactory;
    const t = editor.t;

    componentFactory.add("ipsImageAttach", (locale) => {
      const command = editor.commands.get("attachImage");

      const button = new ButtonView(locale);

      button.set({
        label: t("Attach image"),
        icon: imageIcon,
        tooltip: true,
      });

      button.bind("isEnabled").to(command);

      button.on("execute", () => {
        editor.execute("attachImage");
      });

      return button;
    });
  }
}
