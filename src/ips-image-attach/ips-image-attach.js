import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import IpsImageAttachUi from "./ips-image-attach-ui";
import IpsImageAttachEditing from "./ips-image-attach-editing";

/**
 * This plugin creates an attach image dialog
 */
export default class IpsImageAttach extends Plugin {
  static get requires() {
    return [IpsImageAttachEditing, IpsImageAttachUi];
  }
}
