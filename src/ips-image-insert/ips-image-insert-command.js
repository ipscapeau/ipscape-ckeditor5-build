import Command from "@ckeditor/ckeditor5-core/src/command";

export default class IpsImageInsertCommand extends Command {
  execute() {
    const event = new CustomEvent("on-editor-ips-insert-image", {
      bubbles: true,
    });
    document.dispatchEvent(event);
  }
}
