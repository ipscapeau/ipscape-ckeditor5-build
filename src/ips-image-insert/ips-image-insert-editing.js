import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import IpsImageInsertCommand from "./ips-image-insert-command";

export default class IpsImageInsertEditing extends Plugin {
  init() {
    this.editor.commands.add(
      "insertImage",
      new IpsImageInsertCommand(this.editor)
    );
  }
}
