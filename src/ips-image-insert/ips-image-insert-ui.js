import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import ButtonView from "@ckeditor/ckeditor5-ui/src/button/buttonview";

import imageIcon from "./theme/icons/image.svg";

export default class IpsImageInsertUi extends Plugin {
  init() {
    const editor = this.editor;
    const componentFactory = editor.ui.componentFactory;
    const t = editor.t;

    componentFactory.add("ipsImageInsert", (locale) => {
      const command = editor.commands.get("insertImage");

      const button = new ButtonView(locale);

      button.set({
        label: t("Insert image"),
        icon: imageIcon,
        tooltip: true,
      });

      button.bind("isEnabled").to(command);

      button.on("execute", () => {
        editor.execute("insertImage");
        editor.editing.view.focus();
      });

      return button;
    });
  }
}
