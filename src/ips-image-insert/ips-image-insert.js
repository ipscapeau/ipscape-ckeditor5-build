import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import IpsImageInsertUi from "./ips-image-insert-ui";
import IpsImageInsertEditing from "./ips-image-insert-editing";

/**
 * This plugin creates an insert image dialog
 */
export default class IpsImageInsert extends Plugin {
  static get requires() {
    return [IpsImageInsertEditing, IpsImageInsertUi];
  }
}
