import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import IpsSelectFilesDropdownFromLocalCommand from "./ips-select-files-dropdown-from-local-command";
import IpsSelectFilesDropdownFromLibraryCommand from "./ips-select-files-dropdown-from-library-command";

export default class IpsSelectFilesDropdownEditing extends Plugin {
  init() {
    this.editor.commands.add(
      "selectFromLibrary",
      new IpsSelectFilesDropdownFromLibraryCommand(this.editor)
    );
    this.editor.commands.add(
      "selectFromLocal",
      new IpsSelectFilesDropdownFromLocalCommand(this.editor)
    );
  }
}
