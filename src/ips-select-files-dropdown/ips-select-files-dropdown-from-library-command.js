import Command from "@ckeditor/ckeditor5-core/src/command";

export default class IpsSelectFilesDropdownFromLibraryCommand extends Command {
  execute() {
    const event = new CustomEvent("on-editor-ips-attach-image", {
      bubbles: true,
    });
    document.dispatchEvent(event);
  }
}
