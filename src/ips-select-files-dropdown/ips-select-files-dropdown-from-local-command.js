import Command from "@ckeditor/ckeditor5-core/src/command";

export default class IpsSelectFilesDropdownFromLocalCommand extends Command {
  execute(files) {
    const event = new CustomEvent("on-editor-ips-select-files", {
      bubbles: true,
      detail: files,
    });
    document.dispatchEvent(event);
  }
}
