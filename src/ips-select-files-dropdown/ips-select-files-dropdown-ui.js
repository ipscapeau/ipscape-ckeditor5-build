import {
  createDropdown,
  addToolbarToDropdown,
} from "@ckeditor/ckeditor5-ui/src/dropdown/utils";
import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import imageIcon from "./theme/icons/browse-files.svg";
import FileDialogButtonView from "@ckeditor/ckeditor5-upload/src/ui/filedialogbuttonview";
import ButtonView from "@ckeditor/ckeditor5-ui/src/button/buttonview";

export default class IpsSelectFilesDropdownUi extends Plugin {
  init() {
    const editor = this.editor;
    const componentFactory = editor.ui.componentFactory;

    componentFactory.add("ipsSelectFilesDropdown", (locale) => {
      const commandFromLibrary = editor.commands.get("selectFromLibrary");
      const commandFromLocal = editor.commands.get("selectFromLocal");

      const dropdownView = createDropdown(locale);
      dropdownView.buttonView.set({
        icon: imageIcon,
        label: "Select files",
        withText: false,
      });

      // ddd
      const buttons = [];

      const buttonLibrary = new ButtonView(locale);
      buttonLibrary.set({
        label: "From library",
        tooltip: true,
        withText: true,
      });
      buttonLibrary.bind("isEnabled").to(commandFromLibrary);
      buttonLibrary.on("execute", () => {
        editor.execute("selectFromLibrary");
      });

      const buttonLocal = new FileDialogButtonView(locale);
      buttonLocal.set({
        acceptedType: "*/*",
        allowMultipleFiles: true,
      });
      buttonLocal.buttonView.set({
        label: "From local device",
        tooltip: true,
        withText: true,
      });
      buttonLocal.bind("isEnabled").to(commandFromLocal);
      buttonLocal.on("done", (evt, files) => {
        editor.execute("selectFromLocal", Array.from(files));
        dropdownView.isOpen = false;
      });

      buttons.push(buttonLibrary);
      buttons.push(buttonLocal);
      addToolbarToDropdown(dropdownView, buttons);
      dropdownView.toolbarView.isVertical = true;

      return dropdownView;
    });
  }
}
