import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import IpsSelectFilesDropdownUi from "./ips-select-files-dropdown-ui";
import IpsSelectFilesDropdownEditing from "./ips-select-files-dropdown-editing";

/**
 * This plugin creates an insert image dialog
 */
export default class IpsSelectFilesDropdown extends Plugin {
  static get requires() {
    return [IpsSelectFilesDropdownEditing, IpsSelectFilesDropdownUi];
  }
}
