import Command from "@ckeditor/ckeditor5-core/src/command";

export default class IpsSystemVariablesCommand extends Command {
  execute(systemVariable) {
    const editor = this.editor;

    editor.model.change((writer) => {
      const insertPosition = editor.model.document.selection.getFirstPosition();
      writer.insertText(systemVariable.name, insertPosition);
    });
  }
}
