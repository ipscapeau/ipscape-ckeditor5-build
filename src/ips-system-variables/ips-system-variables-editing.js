import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import IpsSystemVariablesCommand from "./ips-system-variables-command";

export default class IpsSystemVariablesEditing extends Plugin {
  init() {
    this.editor.commands.add(
      "insertSystemVariable",
      new IpsSystemVariablesCommand(this.editor)
    );
  }
}
