import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import {
  addListToDropdown,
  createDropdown,
} from "@ckeditor/ckeditor5-ui/src/dropdown/utils";
import Model from "@ckeditor/ckeditor5-ui/src/model";
import Collection from "@ckeditor/ckeditor5-utils/src/collection";

export default class IpsSystemVariablesUi extends Plugin {
  init() {
    const editor = this.editor;

    editor.ui.componentFactory.add("ipsSystemVariables", (locale) => {
      const command = editor.commands.get("insertSystemVariable");

      // create dropdown
      const dropdownView = new createDropdown(locale);
      dropdownView.buttonView.set({
        label: "System variables",
        withText: true,
      });

      // listener for event "execute"
      this.listenTo(dropdownView, "execute", (evt) => {
        editor.execute("insertSystemVariable", evt.source.systemVariable);
        editor.editing.view.focus();
      });

      // listener for event "change:isOpen"
      this.listenTo(dropdownView, "change:isOpen", (evt) => {
        // when opening dropdown set dynamic field
        if (evt.source.isOpen) {
          // clear dropdown items
          if (dropdownView.panelView) {
            dropdownView.panelView.children.clear();
          }

          // create dropdown items
          const dropdownItems = new Collection();

          // create dropdown item for each object in systemVariables array from editor config
          if (
            editor.config.get("systemVariables") &&
            editor.config.get("systemVariables").length > 0
          ) {
            for (const systemVariable of editor.config.get("systemVariables")) {
              const dropdownItemDefinition = {
                type: "button",
                model: new Model({
                  label: systemVariable.label,
                  withText: true,
                }),
              };
              dropdownItemDefinition.model
                .bind("isOn", "isEnabled")
                .to(command, "value", "isEnabled");
              dropdownItemDefinition.model.set({
                systemVariable: systemVariable,
              });

              // Add the dropdownItemDefinition to the collection.
              dropdownItems.add(dropdownItemDefinition);
            }
          } else {
            const dropdownItemDefinition = {
              type: "button",
              model: new Model({
                label: "-- No fields available --",
                withText: true,
              }),
            };
            dropdownItems.add(dropdownItemDefinition);
          }

          // add items to dropdown
          addListToDropdown(dropdownView, dropdownItems);
        }
      });

      return dropdownView;
    });
  }
}
