import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import IpsSystemVariablesUi from "./ips-system-variables-ui";
import IpsSystemVariablesEditing from "./ips-system-variables-editing";

/**
 * This plugin creates a system variables dropdown
 */
export default class IpsSystemVariables extends Plugin {
  static get requires() {
    return [IpsSystemVariablesEditing, IpsSystemVariablesUi];
  }
}
